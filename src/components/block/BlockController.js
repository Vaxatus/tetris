import Rectangle from "../rectangle/Rectangle.js";
import {FIELD_SIZE, TYPE} from "../GlobalDataEnume.js";
import {clearContextBoard, clearContextField, getBoardPosition} from "../Helper.js";

class BlockController {
    createInitField () {
        this.initField = new Rectangle(this.posX, this.posY, FIELD_SIZE, FIELD_SIZE)
        this.initField.image = new Image()
        this.initField.drawImage = this.drawImage
    }
    createFields () {
        this.current_blockRef = []
        this.current_block = []
        for (let i = 0; i < this.current_block_position.length; i++) {
            this.current_block[i] = []
            for (let j = 0; j < this.current_block_position[i].length; j++) {
                if(this.current_block_position[i][j] === 1) {
                    const x = this.posX + j*FIELD_SIZE
                    const y = this.posY + i*FIELD_SIZE
                    this.current_block[i][j] = new Rectangle(x, y, FIELD_SIZE, FIELD_SIZE)
                    this.decoreField(this.current_block[i][j])
                }
            }
        }
    }
    decoreField (field) {
        field.image = new Image()
        field.drawImage = this.drawImage.bind(this)
        this.current_blockRef.push(field)
    }
    move (x = 0 , y = 0) {
        const context = this.canvas.getContext("2d")
        for (let i = this.current_block_position.length-1; i >= 0; i--) {
            for (let j = this.current_block_position[i].length-1; j >= 0; j--) {
                if (this.current_block && this.current_block[i][j]) {
                    context.clearRect(this.current_block[i][j].left, this.current_block[i][j].top, FIELD_SIZE, FIELD_SIZE);
                    this.current_block[i][j].left += x
                    this.current_block[i][j].top += y
                }
            }
        }
        this.posX += x
        this.posY += y
    }

    /* get next blockIndex and update*/
    rotate (nextCordsPositions, blockIndex) {

        this.current_blockRef.forEach(block => {clearContextField(this.canvas, block)})
    
        this.current_block_position = nextCordsPositions
        this.blockIndex = blockIndex
        this.createFields()
    }

    switchType (canvas, data, context) {
        const { startX, startY } = data

        this.canvas = canvas
        this.context = context
        this.posX = startX
        this.posY = startY
        this.blockType = TYPE.SIMPLE
        this.initField = null
        this.createFields()

    }
}
export default BlockController;
