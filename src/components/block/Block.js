import {FIELD_SIZE, PATH, TYPE} from "../GlobalDataEnume.js";
import BlockController from "./BlockController.js";
import {BLOCK_POSITION} from "./BlockDataEnume.js";
import {getBlockPositionCords} from "../Helper.js";

class Block extends BlockController {

    constructor(canvas, data) {
        super();
        const { startX, startY, color, blockIndex} = data

        this.canvas = canvas
        this.posX = startX
        this.posY = startY
        this.color = color.toUpperCase()
        this.blockIndex = blockIndex // ishape index in rotation
        this.blockType = TYPE.FORM // type by form image or single squers

        this.current_block_position = getBlockPositionCords(this.color, this.blockIndex)
        this.createInitField()
    }

    get blocks () { return this.current_blockRef }

    getImagePath () {
        const { [this.color]: { num, index } } = BLOCK_POSITION
        return this.blockType === TYPE.FORM
            ? PATH.BLOCK_FORM.replace('${color}', `${num}${index[this.blockIndex]}`)
            : PATH.BLOCK.replace('${color}', this.color)
    }

    /* keep context - fix issue with losing context on board */
    drawImage (rectangle, context) {
        const image = rectangle.image
        image.src = this.getImagePath()
        image.onload = () => context.drawImage(image, rectangle.left, rectangle.top)
    }

    draw (context) {
        if (this.blockType === TYPE.FORM) {
            this.drawImage(this.initField, context)
        } else {
            for (let i = 0; i < this.current_block_position.length; i++) {
                for (let j = 0; j < this.current_block_position[i].length; j++) {
                    if(this.current_block[i][j]) {
                        this.drawImage(this.current_block[i][j], context)
                    }
                }
            }
        }
    }
}

export default Block
