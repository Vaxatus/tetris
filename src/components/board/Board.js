import {BLOCK_COLORS, BOARD_INIT_BLOCK_POS, gameStates, PATH, TYPE} from "../GlobalDataEnume.js";
import Block from "../block/Block.js";
import {BLOCK_POSITION} from "../block/BlockDataEnume.js";
import {getBlockPositionCords, pickRandomProperty} from "../Helper.js";

class Board extends EventTarget {
    constructor(canvas) {
        super();
        this.startStatus = gameStates.STOPPED
        this.background = new Image()
        this.setListeners(canvas)
    }
    setListeners (canvas) {
        this.addEventListener('prepareGame', (event) => {
            this.startStatus = event.detail.gameState
            this.startBlock = undefined
            this.generateBlock(canvas)
        })
        this.addEventListener('setGameState', (event) => {this.startStatus = event.detail.gameState})
        this.addEventListener('updateHudState', (event) => {
            if (event.detail.gameFinished) {}
        })
        this.addEventListener('getBlock', (event) => {
            if(this.startBlock){
                const block = Object.assign(Object.create(Object.getPrototypeOf(this.startBlock)), this.startBlock)
                this.startBlock = undefined
                this.dispatchEvent(new CustomEvent('sentBlock', { detail: { block } }))
                this.generateBlock(canvas)
            }
        })
    }

    generateRandomData () {
        const color = pickRandomProperty(BLOCK_COLORS)
        return {
            color,
            blIndex: Math.floor(Math.random() * BLOCK_POSITION[color].index.length)
        }
    }
    generateBlock (canvas) {
        if (this.startBlock) return
        const { color, blIndex } = this.generateRandomData()

        this.startBlock = new Block(canvas, {
            startX: BOARD_INIT_BLOCK_POS.startX,
            startY: BOARD_INIT_BLOCK_POS.startY,
            color: color,
            blockIndex: blIndex,
            blockType: TYPE.FORM
        })
        this.startBlock.image = new Image()
        const height = getBlockPositionCords(color, blIndex).length
        this.dispatchEvent(new CustomEvent('getNewBlockHeight', { detail: { height } }))
    }
    drawBackground (context) {
        this.background.onload = () => {context.drawImage(this.background, 0, 0)}
        this.background.src = PATH.BACKGROUND
    }
    draw (context) {
        this.drawBackground(context)
        if ([gameStates.PREPARE, gameStates.STARTED].includes(this.startStatus) && this.startBlock) {
            this.startBlock.draw(context)
        }
    }
}
export default Board;
