export const FIELD_SIZE = 42

export const TYPE = {
    SIMPLE: "block",
    FORM: "form"
};

export const BLOCK_COLORS = {
    BLUE: "blue",
    GREEN: "green",
    LIGHT_BLUE: "light_blue",
    ORANGE: "orange",
    PINK: "pink",
    RED: "red",
    VIOLET: "violet",
    YELLOW: "yellow"
}

export const PATH = {
    BACKGROUND: "src/components/board/assets/bg_full.jpg",
    BLOCK: "src/components/block/assets/block_${color}.png",
    BLOCK_FORM: "src/components/block/assets/blocks_form/form_${color}.png"
}
export const BOARD_START_POS = {
    startX: 10,
    startY: 110
}
export const BLOCK_START_POS = {
    startX: BOARD_START_POS.startX + 4*FIELD_SIZE,
    startY: BOARD_START_POS.startY
}
export const BOARD_INIT_BLOCK_POS = {
    startX: 470,
    startY: 720
}

export const gameStates = {
    PAUSED: 'PAUSED',
    STOPPED: 'STOPPED',
    STARTED: 'STARTED',
    PREPARE: 'PREPARE'
}
export const TEXTS = { // TODO create files with languages
    START: 'START GAME',
    RESTART: 'RESTART GAME',
}
