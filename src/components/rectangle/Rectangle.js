const textSizeNormal = "20pt sans-serif"
const textSizeSmall = "10pt sans-serif"
class Rectangle extends EventTarget{

    constructor (
        x = 0, y = 0,
        width = 0, height = 0,
        fillColor = '', strokeColor = '', strokeWidth = 2, text = ''
    ) {
        super();
        this.x = Number(x)
        this.y = Number(y)
        this.width = Number(width)
        this.height = Number(height)
        this.fillColor = fillColor
        this.strokeColor = strokeColor
        this.strokeWidth = strokeWidth
        this.text = text
        this.initListeners()

    }

    initListeners () {
        if (this.text) {
            document.addEventListener("click", (event) => this.onMouseClick(event));
        }
    }
    onMouseClick (event) {
        const clickX = event.layerX;
        const clickY = event.layerY;
        if (clickX > this.left &&
            clickX < this.right &&
            clickY > this.top &&
            clickY < this.bottom
            )
        {
            return this.dispatchEvent(new CustomEvent('click'), {
                detail : {
                    id: this.text
                }
            })
        }
    }

    get left () {
        return this.x
    }

    set left (x) {
        this.x = x
    }

    get right () {
        return this.x + this.width
    }

    get top () {
        return this.y
    }

    set top (y) {
        this.y = y
    }
    get bottom () {
        return this.top + this.height
    }
    draw (context) {
        let textFont = textSizeNormal
        /*for debug mode*/
        // if(!this.text) {
        //     const [x, y] = getBoardPosition(this.x, this.y)
        //     this.text = `[${x}, ${y}]`
        //     textFont = textSizeSmall
        // }
        const {
            x, y, width, height,
            fillColor, strokeColor, strokeWidth
        } = this

        // saves the current styles set elsewhere
        // to avoid overwriting them
        context.save()

        // set the styles for this shape
        context.fillStyle = fillColor
        context.lineWidth = strokeWidth

        // create the *path*
        context.beginPath()
        context.strokeStyle = strokeColor
        context.rect(x, y, width, height)

        // draw the path to screen
        context.fill()
        context.stroke()

        if(this.text) {
            context.fillStyle = "white";
            context.font = textFont;
            const x = this.left + this.width*0.08
            const y = this.top + this.height*0.65
            context.fillText(this.text, x, y);
        }


        // restores the styles from earlier
        // preventing the colors used here
        // from polluting other drawings
        context.restore()
    }
}

export default Rectangle
