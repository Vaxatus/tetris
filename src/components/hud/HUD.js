import Rectangle from "../rectangle/Rectangle.js";
import {gameStates, TEXTS} from "../GlobalDataEnume.js";

class HUD extends EventTarget {

    constructor(canvas) {
        super();
        this.canvas = canvas
        this.countDown = 3
        this.gameStatus = {
            showMenu: true,
            showCountDown: false,
            gameFinished: false,
            currentScore: 0
        }

        this.setListeners()
    }

    setListeners () {
        this.addEventListener('updateHudState', (event) => {
            if (event.detail.gameFinished) {
                this.gameStatus.showMenu = true
                this.gameStatus.gameFinished = true
                this.countDown = 3
                this.startButton.addEventListener('click', this.onButtonClick)
            }
        })

        this.startButton = new Rectangle(200, 400, 200, 60, 'purple', 'white', 5, TEXTS.START )
        this.startButton.addEventListener('click', this.onButtonClick)

    }
    onButtonClick = event => {
        this.startButton.removeEventListener('click', this.onButtonClick)
        this.gameStatus.showMenu = false
        this.dispatchEvent(new CustomEvent('prepareGame', { detail: {
                gameState: gameStates.PREPARE,
                restart: this.gameStatus.gameFinished
            }}))
        this.gameStatus.gameFinished = false
        this.startCountDown()
    }
    startCountDown () {
        console.log(this.countDown)
        setTimeout(() => {
            if(this.countDown > 0 ){
                this.countDown -= 1
                this.startCountDown()
            } else {
                console.log('START')
                this.gameStatus.showCountDown = false
                this.dispatchEvent(new CustomEvent('setGameState', { detail: {gameState: gameStates.STARTED}}))
            }
        }, 1000)
    }

    draw(context) {
        context.clearRect(195, 395, 210, 70);

        if (this.gameStatus.showMenu) {
            this.startButton.draw(context)
        } else if(this.gameStatus.showCountDown) {
            context.fillStyle = "purple";
            context.font = "40pt sans-serif";
            context.clearRect(300, 300, 50, 50);
            context.fillText(this.countDown, 300, 300);
        }
    }
}
export default HUD;
