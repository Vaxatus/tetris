import Rectangle from "../rectangle/Rectangle.js";
import {BOARD_START_POS, FIELD_SIZE} from "../GlobalDataEnume.js";
import {boardHeight, boardWidth, KEYS, MOVE_DOWN_TIME} from "./GameDataEnume.js";
import {BLOCK_POSITION} from "../block/BlockDataEnume.js";
import {clearContextBoard, clearContextField, getBlockPositionCords, getBoardPosition} from "../Helper.js";

class GameController extends EventTarget {

    constructor(canvas) {
        super();
        this.canvas = canvas;
    }

    start () {
        if (this.canStartGame()) {
            this.dispatchEvent(new CustomEvent('getBlock'))
        } else {
            this.canMove = false
            this.currentBlock = undefined
            this.gameFinished = true
            this.dispatchEvent(new CustomEvent('updateHudState', {detail: { gameFinished: this.gameFinished }}))
            clearContextBoard(this.canvas)
            this.createBoardFields()
            console.log('THE END')
        }
    }

    clearBoard () {
        const context = this.canvas.getContext("2d")
        for (let i = 0; i < boardWidth; i ++) {
            for (let j = 0; j < boardHeight; j ++) {
                const field = this.board[i][j]
                if(field) {
                    context.clearRect(field.left, field.top, field.width, field.height);
                }
            }
        }
    }

    canStartGame () { // make it better - verify by shape
        for (let i = 0; i < 4; i++) {
            if (this.board[i+4][0].image){
                return false
            }
        }
        return true
    }

    createBoardFields () {
        this.board = []
        for (let i = 0; i < boardWidth; i ++) {
            this.board[i] = []
            for (let j = 0; j < boardHeight; j ++) {
                const x = BOARD_START_POS.startX + i * FIELD_SIZE
                const y = BOARD_START_POS.startY + j * FIELD_SIZE
                this.board[i][j] = new Rectangle(x, y, FIELD_SIZE, FIELD_SIZE,'yellow','black',3)
            }
        }
    }

    startLogic () {
        this.canMove = true
        this.moveTime = setTimeout(() => {
            if (this.canGo(KEYS.DOWN)) {
                this.canMove = false
                this.currentBlock.move(0, FIELD_SIZE)
                this.startLogic()
            } else {
                this.moveBlockToBoard()
            }
        }, MOVE_DOWN_TIME)
    }

    moveBlockToBoard () {
        if(this.gameFinished || !this.currentBlock) return
        this.canMove = false
        const currentBlocks = [...this.currentBlock.blocks]
        this.currentBlock = null
        for (const index in currentBlocks) {
            const [x, y] = getBoardPosition(currentBlocks[index].left, currentBlocks[index].top)
            this.board[x][y] = currentBlocks[index]
        }
        this.removeFullLines()
        this.start()
    }

    tryMoveBlock (event) {
        clearTimeout(this.moveTime);
        switch (event.code) {
            case KEYS.LEFT:
                if (this.canGo(KEYS.LEFT)) {
                    this.currentBlock.move(-FIELD_SIZE, 0);
                }
                this.startLogic()
                break;
            case KEYS.RIGHT:
                if (this.canGo(KEYS.RIGHT)) {
                    this.currentBlock.move(FIELD_SIZE, 0);
                }
                this.startLogic()
                break;
            case KEYS.DOWN:
                if (this.canGo(KEYS.DOWN)) {
                    this.currentBlock.move(0, FIELD_SIZE);
                    this.startLogic()
                } else {
                    clearTimeout(this.moveTime);
                    this.moveBlockToBoard();
                }
                break;
            case KEYS.ROTATE:
                if (this.canGo(KEYS.ROTATE)) {
                    this.rotateBlock()
                }
                this.startLogic()
                break;
            default:
                break;
        }
    }

    rotateBlock () {
        this.currentBlock.rotate(this.nextCordsPositions, this.nextCordsIndex)
        this.nextCordsIndex = undefined
        this.nextCordsPositions = undefined
    }

    getNextBlockIndex (color) {
        return this.currentBlock.blockIndex === BLOCK_POSITION[color].index.length-1 ? 0 : this.currentBlock.blockIndex+1
    }

    validMove (key) {
        if(key === KEYS.ROTATE){
           return this.getMoveValidator([])[key]()
        }

        const currentBlocks = this.currentBlock.blocks
        for (const index in currentBlocks) {
            const position = getBoardPosition(currentBlocks[index].left, currentBlocks[index].top)
            if (this.getMoveValidator(position)[key]()) {
                return false
            }
        }
        return true
    }

    getMoveValidator (positon) {
        const [x, y] = positon
        return {
            [KEYS.LEFT]: () => x === 0 || this.board[x-1][y] && this.board[x-1][y].image,
            [KEYS.RIGHT]: () => x + 1 === boardWidth || this.board[x+1][y] && this.board[x+1][y].image,
            [KEYS.DOWN]: () => y + 1 === boardHeight || this.board[x][y+1] && this.board[x][y+1].image,
            [KEYS.ROTATE]: () => this.rotateMoveValidator()
        }
    }

    rotateMoveValidator () {
        const color = this.currentBlock.color
        if(BLOCK_POSITION[color].index.length === 1) {
            return false
        }
        this.nextCordsIndex = this.getNextBlockIndex(color)
        if (!this.nextCordsPositions) {
            this.nextCordsPositions = getBlockPositionCords(color, this.nextCordsIndex)
        }
        const [x, y] = getBoardPosition(this.currentBlock.posX, this.currentBlock.posY)

        if (x+this.nextCordsPositions[0].length > boardWidth || y+this.nextCordsPositions.length > boardHeight ) {
            return false
        }
        for (let i = 0; i < this.nextCordsPositions.length; i++) {
            for (let j = 0; j < this.nextCordsPositions[i].length; j++) {
                if(this.nextCordsPositions[i][j] === 1) {
                    const field = this.board[x+j][y+i]
                    if (!!field && field.image ) {
                        return false
                    }
                }
            }
        }
        return true
    }

    findFullRows () {
        const rowsToRemove = []

        const isFull = (row) => {
            for (let col = boardWidth-1; col >=0; col--){
                if(!this.board[col][row].image) {
                    return false
                }
            }
            return true
        }
        for (let row = boardHeight-1; row>=0; row--) {
            if(isFull(row)){
                rowsToRemove.push(row)
            }
        }
        return rowsToRemove
    }

    cleanField (field) {
        clearContextField(this.canvas, field)
        field.image = undefined
        field.drawImage = undefined
    }

    removeFullLines () {
        const rowsToRemove = this.findFullRows()
        if (rowsToRemove.length) {
            rowsToRemove.forEach(row => {
                for (let col = boardWidth-1; col >=0; col--){
                    this.cleanField(this.board[col][row])
                }
            })
            this.moveAllDown(rowsToRemove)
            return true
        }
        return false
    }

    setImage (target, image, drawImage) {
        target.image = image ? image : undefined
        target.drawImage = drawImage ? drawImage : undefined
        return target
    }

    swapFields (col, row) {
        const image = this.board[col][row].image
        const drawImage = this.board[col][row].drawImage
        this.board[col][row] = this.setImage(this.board[col][row], this.board[col][row-1].image, this.board[col][row-1].drawImage)
        this.board[col][row-1] = this.setImage(this.board[col][row-1], image, drawImage)
        this.swapOrClean(col, row-1)
    }

    swapOrClean (col, row) {
        if (row-1 > 0) {
            this.swapFields(col, row)
        } else {
            this.cleanField(this.board[col][row])
        }
    }

    moveAllDown (rowsToRemove) {
        if (rowsToRemove.length) {
            rowsToRemove.reverse().forEach(row => {
                for (let col = boardWidth-1; col >=0; col--){
                    this.swapOrClean(col, row)
                }
            })
        }
    }

    canGo (key) {
        if (!this.currentBlock || !this.canMove){
            return false
        }

        switch (key) {
            case KEYS.LEFT:
                return this.validMove(KEYS.LEFT)
            case KEYS.RIGHT:
                return this.validMove(KEYS.RIGHT)
            case KEYS.DOWN:
                return this.validMove(KEYS.DOWN)
            case KEYS.ROTATE:
                return this.validMove(KEYS.ROTATE)
            default:
                break;
        }
    }

}
export default GameController;
