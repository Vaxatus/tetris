import GameController from "./GameController.js";
import {boardHeight, boardWidth} from "./GameDataEnume.js";
import {BLOCK_START_POS, FIELD_SIZE, gameStates as gameStatus, gameStates, TYPE} from "../GlobalDataEnume.js";
import {clearContextBoard} from "../Helper.js";

class Game extends GameController {
    constructor(canvas) {
        super(canvas)
        this.startStatus = false
        this.gameStatus = gameStatus.STOPPED
        this.createBoardFields()
        this.keysLogic()
        this.setListeners(canvas)
    }

    setListeners (canvas) {
        this.addEventListener('prepareGame', (event) => {
            this.startStatus = event.detail.gameState
            if (event.detail.restart) {
                this.gameFinished = false
                this.currentBlock = undefined
                this.canMove = false
                clearTimeout(this.moveTime);
                this.clearBoard()
            }
        })
        this.addEventListener('setGameState', (event) => {
            this.startStatus = event.detail.gameState
            this.start()
        })

        this.addEventListener('sentBlock', (event) => {
            this.nextRotateBlock = false
            this.currentBlock = event.detail.block
            this.currentBlock.switchType(canvas,{
                startX: BLOCK_START_POS.startX,
                startY: BLOCK_START_POS.startY,
                blockType: TYPE.SIMPLE
            })
            this.startLogic()
        })
        this.addEventListener('getNewBlockHeight', (event) => {
            this.nextBlockHeight = event.detail.height
        })
    }
    keysLogic () {
        document.addEventListener('keydown',(event) => {
            this.tryMoveBlock(event)
        });
    }

    draw (context) {
        if (this.startStatus === gameStates.STARTED) {
            for (let i = 0; i < boardWidth; i ++) {
                for (let j = 0; j < boardHeight; j ++) {
                    if(this.board[i][j].image) {
                        this.board[i][j].drawImage(this.board[i][j], context)
                    }
                    // for debug
                    // this.board[i][j].draw(context)
                }
            }
            /* render block */
            if (this.currentBlock) {
                this.currentBlock.draw(context)
            }
        }
    }
}
export default Game;
