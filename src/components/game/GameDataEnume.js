export const boardHeight = 20
export const boardWidth = 10
export const MOVE_DOWN_TIME = 1000
export const KEYS = {
    "LEFT": 'ArrowLeft',
    "RIGHT": 'ArrowRight',
    "DOWN": 'ArrowDown',
    "ROTATE": 'Space'
}
