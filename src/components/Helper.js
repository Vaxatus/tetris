import {BLOCK_POSITION} from "./block/BlockDataEnume.js";
import {BOARD_START_POS, FIELD_SIZE} from "./GlobalDataEnume.js";

export const getBlockPositionCords = (color, blockIndex) => {
    const colorData = BLOCK_POSITION[color]
    const typeIndex = colorData.index[blockIndex]
    return colorData[typeIndex]
}

export const pickRandomProperty = (obj) => {
    let result;
    let count = 0;
    for (const prop in obj)  {
        if (Math.random() < 1/++count) {
            result = prop;
        }
    }
    return result;
}

export const clearContextBoard = (canvas) => {
    const context = canvas.getContext("2d")
    context.clearRect(0, 0, context.width, context.height);
}
export const getBoardPosition = (x, y) => {
    return [(x-BOARD_START_POS.startX)/FIELD_SIZE, (y-BOARD_START_POS.startY)/FIELD_SIZE]
}
/* TODO
*  https://stackoverflow.com/questions/13435959/clearrect-function-doesnt-clear-the-canvas
* */
export const clearContextField = (canvas, field) => {
    const context = canvas.getContext("2d")
    context.canvas.width = context.canvas.width;
    // context.clearRect(field.left, field.top, field.width, field.height);

    // context.beginPath();
    // context.fillStyle = "rgba(0, 0, 0, 0)";
    // context.fillRect(field.left, field.top, field.width, field.height);
    // context.stroke();
}
