import Board from "./components/board/Board.js";
import Game from "./components/game/Game.js";
import HUD from "./components/hud/HUD.js";

class App {

    constructor(layers)
    {
        this.canvas = layers
        this.context = this.canvas.map(cCanvas => cCanvas.getContext("2d"));

        this.board = new Board(this.canvas[0])
        this.game = new Game(this.canvas[1])
        this.hud = new HUD(this.canvas[2])

        this.trackWindowSize();
        this.setListeners()
    }
    setListeners () {
        this.hud.addEventListener('prepareGame', (event) => {
            [this.board, this.game].forEach(item => item.dispatchEvent(new CustomEvent('prepareGame', event)))
        })
        this.hud.addEventListener('setGameState', (event) => {
            [this.board, this.game].forEach(item => item.dispatchEvent(new CustomEvent('setGameState', event)))
        })
        this.board.addEventListener('sentBlock', (event) => {
            this.game.dispatchEvent(new CustomEvent('sentBlock', event))
        })
        this.game.addEventListener('getBlock', (event) => {
            this.board.dispatchEvent(new CustomEvent('getBlock', event))
        })
        this.game.addEventListener('updateHudState', (event) => {
            this.hud.dispatchEvent(new CustomEvent('updateHudState', event))
            this.board.dispatchEvent(new CustomEvent('updateHudState', event))
        })
    }
    nextFrame()
    {
        this.render(this.canvas, this.context);
        requestAnimationFrame(this.nextFrame.bind(this));
    }

    render(canvas, context)
    {
        this.board.draw(context[0])
        this.game.draw(context[1])
        this.hud.draw(context[2])
    }

    /**
     * Updates the canvas size to match the current viewport size.
     */
    matchWindowSize() {
        for (let i = 0; i < this.canvas.length; i++) {
            this.canvas[i].width = window.innerWidth;
            this.canvas[i].height = window.innerHeight;
        }
    }

    /**
     * Makes the canvas size track the window size.
     */
    trackWindowSize() {
        this.matchWindowSize();
        window.addEventListener("resize", this.matchWindowSize.bind(this));
    }
}


export default App;
