import App from "./App.js";

window.addEventListener("load", function (event) {
    const layers = [
        document.getElementById("layer1"),
        document.getElementById("layer2"),
        document.getElementById("layer3")
    ]
    const renderer = new App(layers);
    renderer.nextFrame();
    window.renderer = renderer;
});
