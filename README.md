# RUN
`npm install && npm run start:dev`

### USED TECHNOLOGIES
* ES6
* HTML5
* Canvas
* Webpack

### ISSUES
* clean block (in Helper.js clearContextField)
* remove full line and fall down all blocks (fix after clean block)
* missing score
* start count down is not showing

### CONTROL KEYS
*     "LEFT": 'ArrowLeft'
*     "RIGHT": 'ArrowRight'
*     "DOWN": 'ArrowDown'
*     "ROTATE": 'Space'

### PREVIEW
![image info](./view2.png)
![image info](./view1.png)
